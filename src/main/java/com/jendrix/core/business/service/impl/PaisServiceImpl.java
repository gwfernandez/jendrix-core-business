package com.jendrix.core.business.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.jendrix.common.component.BaseConverter;
import com.jendrix.common.repository.BaseCrudRepository;
import com.jendrix.common.service.impl.BaseCrudService;
import com.jendrix.common.util.Checker;
import com.jendrix.core.business.converter.PaisConverter;
import com.jendrix.core.business.entity.Pais;
import com.jendrix.core.business.model.PaisModel;
import com.jendrix.core.business.repository.PaisRepository;
import com.jendrix.core.business.service.PaisService;
import com.jendrix.core.business.type.TipoFiltro;

@Service("paisServiceImpl")
public class PaisServiceImpl extends BaseCrudService<Pais, PaisModel> implements PaisService {

	@Autowired
	@Qualifier("paisRepository")
	private PaisRepository paisRepository;

	@Autowired
	@Qualifier("paisConverter")
	private PaisConverter paisConverter;

	@Override
	protected final BaseCrudRepository<Pais, Long> getEntityRepository() {
		return this.paisRepository;
	}

	@Override
	protected final BaseConverter<Pais, PaisModel> getEntityConverter() {
		return this.paisConverter;
	}

	@Override
	public final List<PaisModel> findPais(String nombre, TipoFiltro filtro) {

		Iterable<Pais> paises = null;
		if (Checker.isEmpty(nombre)) {
			paises = this.paisRepository.findAll();
		} else {
			if (filtro != null) {
				switch (filtro) {
				case EQUALS:
					paises = this.paisRepository.findByNombre(nombre);
					break;
				case IGNORE_CASE:
					paises = this.paisRepository.findByNombreIgnoreCase(nombre);
					break;
				case CONTAINING:
					paises = this.paisRepository.findByNombreContaining(nombre);
					break;
				case CONTAINING_IGNORE_CASE:
					paises = this.paisRepository.findByNombreContainingIgnoreCase(nombre);
					break;
				case STARTING_WITH:
					paises = this.paisRepository.findByNombreStartingWith(nombre);
					break;
				case STARTING_WITH_IGNORE_CASE:
					paises = this.paisRepository.findByNombreStartingWithIgnoreCase(nombre);
					break;
				default:
					paises = this.paisRepository.findByNombre(nombre);
					break;
				}
			} else {
				paises = this.paisRepository.findByNombre(nombre);
			}
		}

		return getEntityConverter().toModelList(paises);
	}

	@Override
	public final List<PaisModel> findPais(Long continenteId) {
		return findPais(continenteId, null, null);
	}

	@Override
	public final List<PaisModel> findPais(Long continenteId, String nombre, TipoFiltro filtro) {

		Iterable<Pais> paises = null;
		if (Checker.isEmpty(continenteId)) {
			if (Checker.isEmpty(nombre)) {
				paises = this.paisRepository.findAll();
			} else {
				return findPais(nombre, filtro);
			}
		} else {
			if (!Checker.isEmpty(nombre)) {
				if (filtro != null) {
					switch (filtro) {
					case EQUALS:
						paises = this.paisRepository.findByContinente_IdAndNombre(continenteId, nombre);
						break;
					case IGNORE_CASE:
						paises = this.paisRepository.findByContinente_IdAndNombreIgnoreCase(continenteId, nombre);
						break;
					case CONTAINING:
						paises = this.paisRepository.findByContinente_IdAndNombreContaining(continenteId, nombre);
						break;
					case CONTAINING_IGNORE_CASE:
						paises = this.paisRepository.findByContinente_IdAndNombreContainingIgnoreCase(continenteId,
								nombre);
						break;
					case STARTING_WITH:
						paises = this.paisRepository.findByContinente_IdAndNombreStartingWith(continenteId, nombre);
						break;
					case STARTING_WITH_IGNORE_CASE:
						paises = this.paisRepository.findByContinente_IdAndNombreStartingWithIgnoreCase(continenteId,
								nombre);
						break;
					default:
						paises = this.paisRepository.findByContinente_IdAndNombre(continenteId, nombre);
						break;
					}
				} else {
					paises = this.paisRepository.findByContinente_IdAndNombre(continenteId, nombre);
				}
			} else {
				paises = this.paisRepository.findByContinente_Id(continenteId);
			}
		}

		return getEntityConverter().toModelList(paises);
	}
}
