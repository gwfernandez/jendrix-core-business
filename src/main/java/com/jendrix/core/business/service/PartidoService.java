package com.jendrix.core.business.service;

import java.util.List;

import com.jendrix.core.business.model.PartidoModel;
import com.jendrix.core.business.type.TipoFiltro;

public interface PartidoService {

	public List<PartidoModel> findPartido(Long provinciaId);

	public List<PartidoModel> findPartido(Long provinciaId, String nombre, TipoFiltro filtro);
}
