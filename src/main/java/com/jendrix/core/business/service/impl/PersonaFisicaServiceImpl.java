package com.jendrix.core.business.service.impl;

import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.jendrix.common.component.BaseConverter;
import com.jendrix.common.repository.BaseCrudRepository;
import com.jendrix.common.service.impl.BaseCrudService;
import com.jendrix.core.business.converter.PersonaFisicaConverter;
import com.jendrix.core.business.entity.Domicilio;
import com.jendrix.core.business.entity.Pais;
import com.jendrix.core.business.entity.PersonaFisica;
import com.jendrix.core.business.entity.TipoDocumento;
import com.jendrix.core.business.entity.TipoDomicilio;
import com.jendrix.core.business.model.ContinenteModel;
import com.jendrix.core.business.model.PaisModel;
import com.jendrix.core.business.model.PersonaFisicaModel;
import com.jendrix.core.business.repository.ContinenteRepository;
import com.jendrix.core.business.repository.PaisRepository;
import com.jendrix.core.business.repository.PersonaFisicaRepository;
import com.jendrix.core.business.repository.ProvinciaRepository;
import com.jendrix.core.business.repository.TipoDocumentoRepository;
import com.jendrix.core.business.repository.TipoDomicilioRepository;
import com.jendrix.core.business.service.ContinenteService;
import com.jendrix.core.business.service.PaisService;
import com.jendrix.core.business.service.PersonaFisicaService;
import com.jendrix.core.business.type.TipoEstadoCivil;
import com.jendrix.core.business.type.TipoSexo;

@Service("personaFisicaServiceImpl")
public class PersonaFisicaServiceImpl extends BaseCrudService<PersonaFisica, PersonaFisicaModel>
		implements PersonaFisicaService {

	private static final Log LOG = LogFactory.getLog(PersonaFisicaServiceImpl.class);

	@Autowired
	@Qualifier("personaFisicaRepository")
	private PersonaFisicaRepository personaFisicaRepository;

	@Autowired
	@Qualifier("continenteRepository")
	private ContinenteRepository continenteRepository;

	@Autowired
	@Qualifier("paisRepository")
	private PaisRepository paisRepository;

	@Autowired
	@Qualifier("provinciaRepository")
	private ProvinciaRepository provinciaRepository;

	@Autowired
	@Qualifier("tipoDocumentoRepository")
	private TipoDocumentoRepository tipoDocumentoRepository;

	@Autowired
	@Qualifier("tipoDomicilioRepository")
	private TipoDomicilioRepository tipoDomicilioRepository;

	@Autowired
	@Qualifier("personaFisicaConverter")
	private PersonaFisicaConverter personaFisicaConverter;

	@Autowired
	@Qualifier("continenteServiceImpl")
	private ContinenteService continenteService;

	@Autowired
	@Qualifier("paisServiceImpl")
	private PaisService paisService;

	
	@Override
	protected final BaseCrudRepository<PersonaFisica, Long> getEntityRepository() {
		return this.personaFisicaRepository;
	}

	@Override
	protected final BaseConverter<PersonaFisica, PersonaFisicaModel> getEntityConverter() {
		return this.personaFisicaConverter;
	}
	

	@Override
	public void testPopulate() {

		try {
			LOG.info("inicio testPopulate");

			Long argentinaId = 1l;
			Long tipoDocumentoId = 1l;
			Long tipoDomicilioId = 1l;

			Pais argentina = paisRepository.findEntityById(argentinaId);
			TipoDocumento dni = tipoDocumentoRepository.findEntityById(tipoDocumentoId);
			TipoDomicilio particular = tipoDomicilioRepository.findEntityById(tipoDomicilioId);

			PersonaFisica pf = new PersonaFisica();
			pf.setId(0l);
			pf.setTipoDocumento(dni);
			pf.setNumeroDocumento("26468833");

			Domicilio d = new Domicilio();
			d.setTipoDomicilio(particular);
			d.setPais(argentina);
			d.setCalle("Belsky");
			d.setAltura("1132");
			d.setCodigoPostal("1722");
			d.setEntreCalle1("Aristobulo del Valle");
			d.setEntreCalle2("Fleming");
			d.setObservaciones("prueba");

			pf.getDomicilios().add(d);
			d = new Domicilio();
			d.setTipoDomicilio(particular);
			d.setPais(argentina);
			d.setCalle("Beruti");
			d.setAltura("1536");
			d.setCodigoPostal("1722");
			d.setEntreCalle1("Mario Bravo");
			d.setEntreCalle2("Juan B Justo");
			d.setObservaciones("prueba 2");
			pf.getDomicilios().add(d);

			pf.setApellido("Fernandez");
			pf.setNombre("Willy");
			pf.setSexo(TipoSexo.MASCULINO);
			pf.setEstadoCivil(TipoEstadoCivil.CASADO);
			pf.setNacionalidad(argentina);
			pf.setFechaNacimiento(new Date());
			pf.setNumeroTelefono("1130647780");
			pf.setEmail("gwfernandez@gmail.com");

			pf = this.personaFisicaRepository.save(pf);

			// pf.getDomicilios().remove(0);
			// pf = this.personaFisicaRepository.save(pf);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void testPopulate1() {

		try {

			PersonaFisicaModel p = findById(1L);
			p.getDomicilios().remove(0);
			save(p);
			
			List<ContinenteModel> c = this.continenteService.findContinente("europa");
			LOG.info("Continente: " + c);
			c = this.continenteService.findContinente("EUROPA");
			LOG.info("Continente: " + c);
			c = this.continenteService.findContinente("e");;
			LOG.info("Continente: " + c);
			
			
			Iterable<Pais> pa = this.paisRepository.findByContinente_Id(5l);
			LOG.info("Pais: " + pa);
			List<PaisModel> models = this.paisService.findPais(5l);
			LOG.info("Pais: " + models);
			
			PersonaFisica per = this.personaFisicaRepository.findByTipoDocumento_IdAndNumero(1l, "26468833");
			LOG.info(per);
			
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
