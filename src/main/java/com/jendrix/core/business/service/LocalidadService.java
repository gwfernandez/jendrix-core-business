package com.jendrix.core.business.service;

import java.util.List;

import com.jendrix.core.business.model.LocalidadModel;
import com.jendrix.core.business.type.TipoFiltro;

public interface LocalidadService {

	public List<LocalidadModel> findLocalidad(Long partidoId);

	public List<LocalidadModel> findLocalidad(Long partidoId, String nombre, TipoFiltro filtro);

}
