package com.jendrix.core.business.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.jendrix.common.component.BaseConverter;
import com.jendrix.common.repository.BaseCrudRepository;
import com.jendrix.common.service.impl.BaseCrudService;
import com.jendrix.common.util.Checker;
import com.jendrix.core.business.converter.TipoDomicilioConverter;
import com.jendrix.core.business.entity.TipoDomicilio;
import com.jendrix.core.business.model.TipoDomicilioModel;
import com.jendrix.core.business.repository.TipoDomicilioRepository;
import com.jendrix.core.business.service.TipoDomicilioService;
import com.jendrix.core.business.type.TipoFiltro;

@Service("tipoDomicilioServiceImpl")
public class TipoDomicilioServiceImpl extends BaseCrudService<TipoDomicilio, TipoDomicilioModel>
		implements TipoDomicilioService {

	@Autowired
	@Qualifier("tipoDomicilioRepository")
	private TipoDomicilioRepository tipoDomicilioRepository;

	@Autowired
	@Qualifier("tipoDomicilioConverter")
	private TipoDomicilioConverter tipoDomicilioConverter;

	@Override
	protected final BaseCrudRepository<TipoDomicilio, Long> getEntityRepository() {
		return this.tipoDomicilioRepository;
	}

	@Override
	protected final BaseConverter<TipoDomicilio, TipoDomicilioModel> getEntityConverter() {
		return this.tipoDomicilioConverter;
	}

	@Override
	public final List<TipoDomicilioModel> findTipoDomicilio(Long paisId) {
		return findTipoDomicilio(paisId, null, null);
	}

	@Override
	public final List<TipoDomicilioModel> findTipoDomicilio(Long paisId, String nombre, TipoFiltro filtro) {

		Iterable<TipoDomicilio> tipoDomicilios = null;
		if (!Checker.isEmpty(paisId)) {
			if (!Checker.isEmpty(nombre)) {
				if (filtro != null) {
					switch (filtro) {
					case EQUALS:
						tipoDomicilios = this.tipoDomicilioRepository.findByPais_IdAndNombre(paisId, nombre);
						break;
					case IGNORE_CASE:
						tipoDomicilios = this.tipoDomicilioRepository.findByPais_IdAndNombreIgnoreCase(paisId, nombre);
						break;
					case CONTAINING:
						tipoDomicilios = this.tipoDomicilioRepository.findByPais_IdAndNombreContaining(paisId, nombre);
						break;
					case CONTAINING_IGNORE_CASE:
						tipoDomicilios = this.tipoDomicilioRepository.findByPais_IdAndNombreContainingIgnoreCase(paisId,
								nombre);
						break;
					case STARTING_WITH:
						tipoDomicilios = this.tipoDomicilioRepository.findByPais_IdAndNombreStartingWith(paisId,
								nombre);
						break;
					case STARTING_WITH_IGNORE_CASE:
						tipoDomicilios = this.tipoDomicilioRepository
								.findByPais_IdAndNombreStartingWithIgnoreCase(paisId, nombre);
						break;
					default:
						tipoDomicilios = this.tipoDomicilioRepository.findByPais_IdAndNombre(paisId, nombre);
						break;
					}
				} else {
					tipoDomicilios = this.tipoDomicilioRepository.findByPais_IdAndNombre(paisId, nombre);
				}
			} else {
				tipoDomicilios = this.tipoDomicilioRepository.findByPais_Id(paisId);
			}
		}

		return getEntityConverter().toModelList(tipoDomicilios);
	}
}
