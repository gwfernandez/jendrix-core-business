package com.jendrix.core.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.jendrix.common.component.BaseConverter;
import com.jendrix.common.repository.BaseCrudRepository;
import com.jendrix.common.service.impl.BaseCrudService;
import com.jendrix.core.business.converter.PersonaJuridicaConverter;
import com.jendrix.core.business.entity.PersonaJuridica;
import com.jendrix.core.business.model.PersonaJuridicaModel;
import com.jendrix.core.business.repository.PersonaJuridicaRepository;
import com.jendrix.core.business.service.PersonaJuridicaService;

@Service("personaJuridicaServiceImpl")
public class PersonaJuridicaServiceImpl extends BaseCrudService<PersonaJuridica, PersonaJuridicaModel>
		implements PersonaJuridicaService {

	@Autowired
	@Qualifier("personaJuridicaRepository")
	private PersonaJuridicaRepository personaJuridicaRepository;

	@Autowired
	@Qualifier("personaJuridicaConverter")
	private PersonaJuridicaConverter personaJuridicaConverter;

	@Override
	protected BaseCrudRepository<PersonaJuridica, Long> getEntityRepository() {
		return this.personaJuridicaRepository;
	}

	@Override
	protected BaseConverter<PersonaJuridica, PersonaJuridicaModel> getEntityConverter() {
		return this.personaJuridicaConverter;
	}

}
