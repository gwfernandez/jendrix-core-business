package com.jendrix.core.business.service;

import java.util.List;

import com.jendrix.core.business.model.ProvinciaModel;
import com.jendrix.core.business.type.TipoFiltro;

public interface ProvinciaService {

	public List<ProvinciaModel> findProvincia(Long paisId);

	public List<ProvinciaModel> findProvincia(Long paisId, String nombre, TipoFiltro filtro);

}
