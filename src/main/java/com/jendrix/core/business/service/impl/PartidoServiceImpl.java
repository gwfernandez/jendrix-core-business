package com.jendrix.core.business.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.jendrix.common.component.BaseConverter;
import com.jendrix.common.repository.BaseCrudRepository;
import com.jendrix.common.service.impl.BaseCrudService;
import com.jendrix.common.util.Checker;
import com.jendrix.core.business.converter.PartidoConverter;
import com.jendrix.core.business.entity.Partido;
import com.jendrix.core.business.model.PartidoModel;
import com.jendrix.core.business.repository.PartidoRepository;
import com.jendrix.core.business.service.PartidoService;
import com.jendrix.core.business.type.TipoFiltro;

@Service("partidoServiceImpl")
public class PartidoServiceImpl extends BaseCrudService<Partido, PartidoModel> implements PartidoService {

	@Autowired
	@Qualifier("partidoRepository")
	private PartidoRepository partidoRepository;

	@Autowired
	@Qualifier("partidoConverter")
	private PartidoConverter partidoConverter;

	@Override
	protected final BaseCrudRepository<Partido, Long> getEntityRepository() {
		return this.partidoRepository;
	}

	@Override
	protected final BaseConverter<Partido, PartidoModel> getEntityConverter() {
		return this.partidoConverter;
	}

	@Override
	public final List<PartidoModel> findPartido(Long provinciaId) {
		return findPartido(provinciaId, null, null);
	}

	@Override
	public final List<PartidoModel> findPartido(Long provinciaId, String nombre, TipoFiltro filtro) {

		Iterable<Partido> partidos = null;
		if (!Checker.isEmpty(provinciaId)) {
			if (!Checker.isEmpty(nombre)) {
				if (filtro != null) {
					switch (filtro) {
					case EQUALS:
						partidos = this.partidoRepository.findByProvincia_IdAndNombre(provinciaId, nombre);
						break;
					case IGNORE_CASE:
						partidos = this.partidoRepository.findByProvincia_IdAndNombreIgnoreCase(provinciaId, nombre);
						break;
					case CONTAINING:
						partidos = this.partidoRepository.findByProvincia_IdAndNombreContaining(provinciaId, nombre);
						break;
					case CONTAINING_IGNORE_CASE:
						partidos = this.partidoRepository.findByProvincia_IdAndNombreContainingIgnoreCase(provinciaId,
								nombre);
						break;
					case STARTING_WITH:
						partidos = this.partidoRepository.findByProvincia_IdAndNombreStartingWith(provinciaId, nombre);
						break;
					case STARTING_WITH_IGNORE_CASE:
						partidos = this.partidoRepository.findByProvincia_IdAndNombreStartingWithIgnoreCase(provinciaId,
								nombre);
						break;
					default:
						partidos = this.partidoRepository.findByProvincia_IdAndNombre(provinciaId, nombre);
						break;
					}
				} else {
					partidos = this.partidoRepository.findByProvincia_IdAndNombre(provinciaId, nombre);
				}
			} else {
				partidos = this.partidoRepository.findByProvincia_Id(provinciaId);
			}
		}

		return getEntityConverter().toModelList(partidos);
	}
}
