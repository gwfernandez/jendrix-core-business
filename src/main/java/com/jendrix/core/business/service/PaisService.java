package com.jendrix.core.business.service;

import java.util.List;

import com.jendrix.core.business.model.PaisModel;
import com.jendrix.core.business.type.TipoFiltro;

public interface PaisService {

	public List<PaisModel> findPais(String nombre, TipoFiltro filtro);

	public List<PaisModel> findPais(Long continenteId);

	public List<PaisModel> findPais(Long continenteId, String nombre, TipoFiltro filtro);

}
