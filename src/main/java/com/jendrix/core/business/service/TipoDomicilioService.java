package com.jendrix.core.business.service;

import java.util.List;

import com.jendrix.core.business.model.TipoDomicilioModel;
import com.jendrix.core.business.type.TipoFiltro;

public interface TipoDomicilioService {

	public  List<TipoDomicilioModel> findTipoDomicilio(Long paisId);

	public List<TipoDomicilioModel> findTipoDomicilio(Long paisId, String nombre, TipoFiltro filtro);

}
