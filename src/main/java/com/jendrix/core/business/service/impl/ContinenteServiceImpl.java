package com.jendrix.core.business.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.jendrix.common.component.BaseConverter;
import com.jendrix.common.repository.BaseCrudRepository;
import com.jendrix.common.service.impl.BaseCrudService;
import com.jendrix.common.util.Checker;
import com.jendrix.core.business.converter.ContinenteConverter;
import com.jendrix.core.business.entity.Continente;
import com.jendrix.core.business.model.ContinenteModel;
import com.jendrix.core.business.repository.ContinenteRepository;
import com.jendrix.core.business.service.ContinenteService;
import com.jendrix.core.business.type.TipoFiltro;

@Service("continenteServiceImpl")
public class ContinenteServiceImpl extends BaseCrudService<Continente, ContinenteModel> implements ContinenteService {

	@Autowired
	@Qualifier("continenteRepository")
	private ContinenteRepository continenteRepository;

	@Autowired
	private ContinenteConverter continenteConverter;

	@Override
	protected final BaseCrudRepository<Continente, Long> getEntityRepository() {
		return this.continenteRepository;
	}

	@Override
	protected final BaseConverter<Continente, ContinenteModel> getEntityConverter() {
		return this.continenteConverter;
	}
	
	@Override
	public final List<ContinenteModel> findContinente(String nombre, TipoFiltro filtro) {

		Iterable<Continente> continentes = null;
		if (Checker.isEmpty(nombre)) {
			continentes = this.continenteRepository.findAll();
		} else {
			// busco el contininente por nombre segun el tipo de filtro indicado
			if (filtro != null) {
				switch (filtro) {
				case EQUALS:
					continentes = this.continenteRepository.findByNombre(nombre);
					break;
				case IGNORE_CASE:
					continentes = this.continenteRepository.findByNombreIgnoreCase(nombre);
					break;
				case CONTAINING:
					continentes = this.continenteRepository.findByNombreContaining(nombre);
					break;
				case CONTAINING_IGNORE_CASE:
					continentes = this.continenteRepository.findByNombreContainingIgnoreCase(nombre);
					break;
				case STARTING_WITH:
					continentes = this.continenteRepository.findByNombreStartingWith(nombre);
					break;
				case STARTING_WITH_IGNORE_CASE:
					continentes = this.continenteRepository.findByNombreStartingWithIgnoreCase(nombre);
					break;
				default:
					continentes = this.continenteRepository.findByNombre(nombre);
					break;
				}
			} else {
				continentes = this.continenteRepository.findByNombre(nombre);
			}
		}

		return getEntityConverter().toModelList(continentes);
	}

	@Override
	public final List<ContinenteModel> findContinente(String nombre) {
		return findContinente(nombre, null);
	}
}
