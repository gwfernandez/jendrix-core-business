package com.jendrix.core.business.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.jendrix.common.component.BaseConverter;
import com.jendrix.common.repository.BaseCrudRepository;
import com.jendrix.common.service.impl.BaseCrudService;
import com.jendrix.common.util.Checker;
import com.jendrix.core.business.converter.TipoDocumentoConverter;
import com.jendrix.core.business.entity.TipoDocumento;
import com.jendrix.core.business.model.TipoDocumentoModel;
import com.jendrix.core.business.repository.TipoDocumentoRepository;
import com.jendrix.core.business.service.TipoDocumentoService;
import com.jendrix.core.business.type.TipoFiltro;

@Service("tipoDocumentoServiceImpl")
public class TipoDocumentoServiceImpl extends BaseCrudService<TipoDocumento, TipoDocumentoModel>
		implements TipoDocumentoService {

	@Autowired
	@Qualifier("tipoDocumentoRepository")
	private TipoDocumentoRepository tipoDocumentoRepository;

	@Autowired
	@Qualifier("tipoDocumentoConverter")
	private TipoDocumentoConverter tipoDocumentoConverter;

	@Override
	protected final BaseCrudRepository<TipoDocumento, Long> getEntityRepository() {
		return this.tipoDocumentoRepository;
	}

	@Override
	protected final BaseConverter<TipoDocumento, TipoDocumentoModel> getEntityConverter() {
		return this.tipoDocumentoConverter;
	}

	@Override
	public final List<TipoDocumentoModel> findTipoDocumento(Long paisId) {
		return findTipoDocumento(paisId, null, null);
	}

	@Override
	public final List<TipoDocumentoModel> findTipoDocumento(Long paisId, String nombre, TipoFiltro filtro) {

		Iterable<TipoDocumento> tipoDocumentos = null;
		if (!Checker.isEmpty(paisId)) {
			if (!Checker.isEmpty(nombre)) {
				if (filtro != null) {
					switch (filtro) {
					case EQUALS:
						tipoDocumentos = this.tipoDocumentoRepository.findByPais_IdAndNombre(paisId, nombre);
						break;
					case IGNORE_CASE:
						tipoDocumentos = this.tipoDocumentoRepository.findByPais_IdAndNombreIgnoreCase(paisId, nombre);
						break;
					case CONTAINING:
						tipoDocumentos = this.tipoDocumentoRepository.findByPais_IdAndNombreContaining(paisId, nombre);
						break;
					case CONTAINING_IGNORE_CASE:
						tipoDocumentos = this.tipoDocumentoRepository.findByPais_IdAndNombreContainingIgnoreCase(paisId,
								nombre);
						break;
					case STARTING_WITH:
						tipoDocumentos = this.tipoDocumentoRepository.findByPais_IdAndNombreStartingWith(paisId, nombre);
						break;
					case STARTING_WITH_IGNORE_CASE:
						tipoDocumentos = this.tipoDocumentoRepository.findByPais_IdAndNombreStartingWithIgnoreCase(paisId,
								nombre);
						break;
					default:
						tipoDocumentos = this.tipoDocumentoRepository.findByPais_IdAndNombre(paisId, nombre);
						break;
					}
				} else {
					tipoDocumentos = this.tipoDocumentoRepository.findByPais_IdAndNombre(paisId, nombre);
				}
			} else {
				tipoDocumentos = this.tipoDocumentoRepository.findByPais_Id(paisId);
			}
		}

		return getEntityConverter().toModelList(tipoDocumentos);
	}
}
