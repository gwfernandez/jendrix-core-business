package com.jendrix.core.business.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.jendrix.common.component.BaseConverter;
import com.jendrix.common.repository.BaseCrudRepository;
import com.jendrix.common.service.impl.BaseCrudService;
import com.jendrix.common.util.Checker;
import com.jendrix.core.business.converter.ProvinciaConverter;
import com.jendrix.core.business.entity.Provincia;
import com.jendrix.core.business.model.ProvinciaModel;
import com.jendrix.core.business.repository.ProvinciaRepository;
import com.jendrix.core.business.service.ProvinciaService;
import com.jendrix.core.business.type.TipoFiltro;

public class ProvinciaServiceImpl extends BaseCrudService<Provincia, ProvinciaModel> implements ProvinciaService {

	@Autowired
	@Qualifier("provinciaRepository")
	private ProvinciaRepository provinciaRepository;

	@Autowired
	@Qualifier("provinciaConverter")
	private ProvinciaConverter provinciaConverter;

	@Override
	protected final BaseCrudRepository<Provincia, Long> getEntityRepository() {
		return this.provinciaRepository;
	}

	@Override
	protected final BaseConverter<Provincia, ProvinciaModel> getEntityConverter() {
		return this.provinciaConverter;
	}
	
	@Override
	public final List<ProvinciaModel> findProvincia(Long paisId) {
		return findProvincia(paisId, null, null);
	}

	@Override
	public final List<ProvinciaModel> findProvincia(Long paisId, String nombre, TipoFiltro filtro) {

		Iterable<Provincia> provincias = null;
		if (!Checker.isEmpty(paisId)) {
			if (!Checker.isEmpty(nombre)) {
				if (filtro != null) {
					switch (filtro) {
					case EQUALS:
						provincias = this.provinciaRepository.findByPais_IdAndNombre(paisId, nombre);
						break;
					case IGNORE_CASE:
						provincias = this.provinciaRepository.findByPais_IdAndNombreIgnoreCase(paisId, nombre);
						break;
					case CONTAINING:
						provincias = this.provinciaRepository.findByPais_IdAndNombreContaining(paisId, nombre);
						break;
					case CONTAINING_IGNORE_CASE:
						provincias = this.provinciaRepository.findByPais_IdAndNombreContainingIgnoreCase(paisId,
								nombre);
						break;
					case STARTING_WITH:
						provincias = this.provinciaRepository.findByPais_IdAndNombreStartingWith(paisId, nombre);
						break;
					case STARTING_WITH_IGNORE_CASE:
						provincias = this.provinciaRepository.findByPais_IdAndNombreStartingWithIgnoreCase(paisId,
								nombre);
						break;
					default:
						provincias = this.provinciaRepository.findByPais_IdAndNombre(paisId, nombre);
						break;
					}
				} else {
					provincias = this.provinciaRepository.findByPais_IdAndNombre(paisId, nombre);
				}
			} else {
				provincias = this.provinciaRepository.findByPais_Id(paisId);
			}
		}

		return getEntityConverter().toModelList(provincias);
	}
}
