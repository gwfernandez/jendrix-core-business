package com.jendrix.core.business.service;

import java.util.List;

import com.jendrix.core.business.model.TipoDocumentoModel;
import com.jendrix.core.business.type.TipoFiltro;

public interface TipoDocumentoService {

	public List<TipoDocumentoModel> findTipoDocumento(Long paisId);

	public List<TipoDocumentoModel> findTipoDocumento(Long paisId, String nombre, TipoFiltro filtro);

}
