package com.jendrix.core.business.service;

import java.util.List;

import com.jendrix.core.business.model.ContinenteModel;
import com.jendrix.core.business.type.TipoFiltro;

public interface ContinenteService {

	public List<ContinenteModel> findContinente(String nombre);

	public List<ContinenteModel> findContinente(String nombre, TipoFiltro filtro);

}
