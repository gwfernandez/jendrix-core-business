package com.jendrix.core.business.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.jendrix.common.component.BaseConverter;
import com.jendrix.common.repository.BaseCrudRepository;
import com.jendrix.common.service.impl.BaseCrudService;
import com.jendrix.common.util.Checker;
import com.jendrix.core.business.converter.LocalidadConverter;
import com.jendrix.core.business.entity.Localidad;
import com.jendrix.core.business.model.LocalidadModel;
import com.jendrix.core.business.repository.LocalidadRepository;
import com.jendrix.core.business.service.LocalidadService;
import com.jendrix.core.business.type.TipoFiltro;

@Service("localidadServiceImpl")
public class LocalidadServiceImpl extends BaseCrudService<Localidad, LocalidadModel> implements LocalidadService {

	@Autowired
	@Qualifier("localidadRepository")
	private LocalidadRepository localidadRepository;

	@Autowired
	@Qualifier("localidadConverter")
	private LocalidadConverter localidadConverter;

	@Override
	protected final BaseCrudRepository<Localidad, Long> getEntityRepository() {
		return this.localidadRepository;
	}

	@Override
	protected final BaseConverter<Localidad, LocalidadModel> getEntityConverter() {
		return this.localidadConverter;
	}

	@Override
	public final List<LocalidadModel> findLocalidad(Long partidoId) {
		return findLocalidad(partidoId, null, null);
	}

	@Override
	public final List<LocalidadModel> findLocalidad(Long partidoId, String nombre, TipoFiltro filtro) {

		Iterable<Localidad> partidos = null;
		if (!Checker.isEmpty(partidoId)) {
			if (!Checker.isEmpty(nombre)) {
				if (filtro != null) {
					switch (filtro) {
					case EQUALS:
						partidos = this.localidadRepository.findByPartido_IdAndNombre(partidoId, nombre);
						break;
					case IGNORE_CASE:
						partidos = this.localidadRepository.findByPartido_IdAndNombreIgnoreCase(partidoId, nombre);
						break;
					case CONTAINING:
						partidos = this.localidadRepository.findByPartido_IdAndNombreContaining(partidoId, nombre);
						break;
					case CONTAINING_IGNORE_CASE:
						partidos = this.localidadRepository.findByPartido_IdAndNombreContainingIgnoreCase(partidoId,
								nombre);
						break;
					case STARTING_WITH:
						partidos = this.localidadRepository.findByPartido_IdAndNombreStartingWith(partidoId, nombre);
						break;
					case STARTING_WITH_IGNORE_CASE:
						partidos = this.localidadRepository.findByPartido_IdAndNombreStartingWithIgnoreCase(partidoId,
								nombre);
						break;
					default:
						partidos = this.localidadRepository.findByPartido_IdAndNombre(partidoId, nombre);
						break;
					}
				} else {
					partidos = this.localidadRepository.findByPartido_IdAndNombre(partidoId, nombre);
				}
			} else {
				partidos = this.localidadRepository.findByPartido_Id(partidoId);
			}
		}

		return getEntityConverter().toModelList(partidos);
	}
}