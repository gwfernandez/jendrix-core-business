package com.jendrix.core.business.model;

import java.io.Serializable;

import com.jendrix.core.business.type.TipoPersona;

@SuppressWarnings("serial")
public final class TipoDomicilioModel implements Serializable {

	private Long id;
	private PaisModel pais;
	private TipoPersona tipoPersona;
	private String nombre;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public PaisModel getPais() {
		return pais;
	}

	public void setPais(PaisModel pais) {
		this.pais = pais;
	}

	public TipoPersona getTipoPersona() {
		return tipoPersona;
	}

	public void setTipoPersona(TipoPersona tipoPersona) {
		this.tipoPersona = tipoPersona;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
}