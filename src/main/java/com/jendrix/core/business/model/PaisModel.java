package com.jendrix.core.business.model;

import java.io.Serializable;

@SuppressWarnings("serial")
public final class PaisModel implements Serializable {

	private Long id;
	private ContinenteModel continente;
	private String nombre;
	private String nombreOficial;
	private String codigoNumerico;
	private String codigoAlfa2;
	private String codigoAlfa3;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ContinenteModel getContinente() {
		return continente;
	}

	public void setContinente(ContinenteModel continente) {
		this.continente = continente;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getNombreOficial() {
		return nombreOficial;
	}

	public void setNombreOficial(String nombreOficial) {
		this.nombreOficial = nombreOficial;
	}

	public String getCodigoNumerico() {
		return codigoNumerico;
	}

	public void setCodigoNumerico(String codigoNumerico) {
		this.codigoNumerico = codigoNumerico;
	}

	public String getCodigoAlfa2() {
		return codigoAlfa2;
	}

	public void setCodigoAlfa2(String codigoAlfa2) {
		this.codigoAlfa2 = codigoAlfa2;
	}

	public String getCodigoAlfa3() {
		return codigoAlfa3;
	}

	public void setCodigoAlfa3(String codigoAlfa3) {
		this.codigoAlfa3 = codigoAlfa3;
	}
}