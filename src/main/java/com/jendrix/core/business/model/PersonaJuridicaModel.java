package com.jendrix.core.business.model;

import java.util.Date;

import com.jendrix.core.business.type.TipoPersona;

@SuppressWarnings("serial")
public class PersonaJuridicaModel extends PersonaModel {

	private String razonSocial;
	private PaisModel nacionalidad;
	private Date fechaInicioActividad;
	private String numeroTelefono;
	private String email;

	public PersonaJuridicaModel() {
		super(TipoPersona.PERSONA_JURIDICA);
	}

	public String getRazonSocial() {
		return razonSocial;
	}

	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}

	public PaisModel getNacionalidad() {
		return nacionalidad;
	}

	public void setNacionalidad(PaisModel nacionalidad) {
		this.nacionalidad = nacionalidad;
	}

	public Date getFechaInicioActividad() {
		return fechaInicioActividad;
	}

	public void setFechaInicioActividad(Date fechaInicioActividad) {
		this.fechaInicioActividad = fechaInicioActividad;
	}

	public String getNumeroTelefono() {
		return numeroTelefono;
	}

	public void setNumeroTelefono(String numeroTelefono) {
		this.numeroTelefono = numeroTelefono;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
}