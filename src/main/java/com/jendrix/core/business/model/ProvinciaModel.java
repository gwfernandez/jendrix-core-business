package com.jendrix.core.business.model;

import java.io.Serializable;

@SuppressWarnings("serial")
public final class ProvinciaModel implements Serializable {

	private Long id;
	private PaisModel pais;
	private String nombre;
	private String abreviatura;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public PaisModel getPais() {
		return pais;
	}

	public void setPais(PaisModel pais) {
		this.pais = pais;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getAbreviatura() {
		return abreviatura;
	}

	public void setAbreviatura(String abreviatura) {
		this.abreviatura = abreviatura;
	}
}