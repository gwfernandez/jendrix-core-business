package com.jendrix.core.business.model;

import java.io.Serializable;

@SuppressWarnings("serial")
public final class LocalidadModel implements Serializable {

	private Long id;
	private PartidoModel partido;
	private String nombre;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public PartidoModel getPartido() {
		return partido;
	}

	public void setPartido(PartidoModel partido) {
		this.partido = partido;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
}