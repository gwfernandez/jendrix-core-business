package com.jendrix.core.business.model;

import java.io.Serializable;
import java.util.List;

import com.jendrix.core.business.type.TipoPersona;

@SuppressWarnings("serial")
public abstract class PersonaModel implements Serializable {

	private Long id;
	private TipoPersona tipoPersona;
	private TipoDocumentoModel tipoDocumento;
	private String numeroDocumento;
	private List<DomicilioModel> domicilios;

	protected PersonaModel (TipoPersona tipoPersona) {
		this.tipoPersona =  tipoPersona;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public TipoPersona getTipoPersona() {
		return tipoPersona;
	}

	public TipoDocumentoModel getTipoDocumento() {
		return tipoDocumento;
	}

	public void setTipoDocumento(TipoDocumentoModel tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	public String getNumeroDocumento() {
		return numeroDocumento;
	}

	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}

	public List<DomicilioModel> getDomicilios() {
		return domicilios;
	}

	public void setDomicilios(List<DomicilioModel> domicilios) {
		this.domicilios = domicilios;
	}
}