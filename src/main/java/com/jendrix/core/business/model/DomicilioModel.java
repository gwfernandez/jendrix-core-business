package com.jendrix.core.business.model;

import java.io.Serializable;

@SuppressWarnings("serial")
public class DomicilioModel implements Serializable {

	private Long id;
	private TipoDomicilioModel tipoDomicilio;
	private PaisModel pais;
	private ProvinciaModel provincia;
	private PartidoModel partido;
	private LocalidadModel localidad;
	private String calle;
	private String altura;
	private String piso;
	private String departamento;
	private String codigoPostal;
	private String entreCalle1;
	private String entreCalle2;
	private String observaciones;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public TipoDomicilioModel getTipoDomicilio() {
		return tipoDomicilio;
	}

	public void setTipoDomicilio(TipoDomicilioModel tipoDomicilio) {
		this.tipoDomicilio = tipoDomicilio;
	}

	public PaisModel getPais() {
		return pais;
	}

	public void setPais(PaisModel pais) {
		this.pais = pais;
	}

	public ProvinciaModel getProvincia() {
		return provincia;
	}

	public void setProvincia(ProvinciaModel provincia) {
		this.provincia = provincia;
	}

	public PartidoModel getPartido() {
		return partido;
	}

	public void setPartido(PartidoModel partido) {
		this.partido = partido;
	}

	public LocalidadModel getLocalidad() {
		return localidad;
	}

	public void setLocalidad(LocalidadModel localidad) {
		this.localidad = localidad;
	}

	public String getCalle() {
		return calle;
	}

	public void setCalle(String calle) {
		this.calle = calle;
	}

	public String getAltura() {
		return altura;
	}

	public void setAltura(String altura) {
		this.altura = altura;
	}

	public String getPiso() {
		return piso;
	}

	public void setPiso(String piso) {
		this.piso = piso;
	}

	public String getDepartamento() {
		return departamento;
	}

	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}

	public String getCodigoPostal() {
		return codigoPostal;
	}

	public void setCodigoPostal(String codigoPostal) {
		this.codigoPostal = codigoPostal;
	}

	public String getEntreCalle1() {
		return entreCalle1;
	}

	public void setEntreCalle1(String entreCalle1) {
		this.entreCalle1 = entreCalle1;
	}

	public String getEntreCalle2() {
		return entreCalle2;
	}

	public void setEntreCalle2(String entreCalle2) {
		this.entreCalle2 = entreCalle2;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}
}