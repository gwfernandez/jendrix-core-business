package com.jendrix.core.business.model;

import java.io.Serializable;

@SuppressWarnings("serial")
public final class ContinenteModel implements Serializable {

	private Long id;
	private String nombre;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
}