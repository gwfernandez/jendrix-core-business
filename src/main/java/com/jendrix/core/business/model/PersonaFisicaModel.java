package com.jendrix.core.business.model;

import java.util.Date;

import com.jendrix.core.business.type.TipoEstadoCivil;
import com.jendrix.core.business.type.TipoPersona;
import com.jendrix.core.business.type.TipoSexo;

@SuppressWarnings("serial")
public class PersonaFisicaModel extends PersonaModel {

	private String apellido;
	private String nombre;
	private TipoSexo sexo;
	private TipoEstadoCivil estadoCivil;
	private PaisModel nacionalidad;
	private Date fechaNacimiento;
	private String numeroTelefono;
	private String email;
	
	public PersonaFisicaModel() {
		super(TipoPersona.PERSONA_FISICA);
	}	

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public TipoSexo getSexo() {
		return sexo;
	}

	public void setSexo(TipoSexo sexo) {
		this.sexo = sexo;
	}

	public TipoEstadoCivil getEstadoCivil() {
		return estadoCivil;
	}

	public void setEstadoCivil(TipoEstadoCivil estadoCivil) {
		this.estadoCivil = estadoCivil;
	}

	public PaisModel getNacionalidad() {
		return nacionalidad;
	}

	public void setNacionalidad(PaisModel nacionalidad) {
		this.nacionalidad = nacionalidad;
	}

	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public String getNumeroTelefono() {
		return numeroTelefono;
	}

	public void setNumeroTelefono(String numeroTelefono) {
		this.numeroTelefono = numeroTelefono;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
}