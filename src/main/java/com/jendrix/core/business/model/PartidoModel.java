package com.jendrix.core.business.model;

import java.io.Serializable;

@SuppressWarnings("serial")
public final class PartidoModel implements Serializable {

	private Long id;
	private ProvinciaModel provincia;
	private String nombre;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ProvinciaModel getProvincia() {
		return provincia;
	}

	public void setProvincia(ProvinciaModel provincia) {
		this.provincia = provincia;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
}