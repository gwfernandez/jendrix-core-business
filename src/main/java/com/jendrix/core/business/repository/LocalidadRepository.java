package com.jendrix.core.business.repository;

import org.springframework.stereotype.Repository;

import com.jendrix.common.repository.BaseCrudRepository;
import com.jendrix.core.business.entity.Localidad;

@Repository("localidadRepository")
public interface LocalidadRepository extends BaseCrudRepository<Localidad, Long> {

	public Iterable<Localidad> findByPartido_Id(Long partidoId);

	public Iterable<Localidad> findByPartido_IdAndNombre(Long partidoId, String nombre);

	public Iterable<Localidad> findByPartido_IdAndNombreIgnoreCase(Long partidoId, String nombre);

	public Iterable<Localidad> findByPartido_IdAndNombreContaining(Long partidoId, String nombre);

	public Iterable<Localidad> findByPartido_IdAndNombreContainingIgnoreCase(Long partidoId, String nombre);

	public Iterable<Localidad> findByPartido_IdAndNombreStartingWith(Long partidoId, String nombre);

	public Iterable<Localidad> findByPartido_IdAndNombreStartingWithIgnoreCase(Long partidoId, String nombre);
}
