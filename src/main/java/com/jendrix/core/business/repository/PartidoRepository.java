package com.jendrix.core.business.repository;

import org.springframework.stereotype.Repository;

import com.jendrix.common.repository.BaseCrudRepository;
import com.jendrix.core.business.entity.Partido;

@Repository("partidoRepository")
public interface PartidoRepository extends BaseCrudRepository<Partido, Long> {

	public Iterable<Partido> findByProvincia_Id(Long provinciaId);

	public Iterable<Partido> findByProvincia_IdAndNombre(Long provinciaId, String nombre);

	public Iterable<Partido> findByProvincia_IdAndNombreIgnoreCase(Long provinciaId, String nombre);

	public Iterable<Partido> findByProvincia_IdAndNombreContaining(Long provinciaId, String nombre);

	public Iterable<Partido> findByProvincia_IdAndNombreContainingIgnoreCase(Long provinciaId, String nombre);

	public Iterable<Partido> findByProvincia_IdAndNombreStartingWith(Long provinciaId, String nombre);

	public Iterable<Partido> findByProvincia_IdAndNombreStartingWithIgnoreCase(Long provinciaId, String nombre);
}
