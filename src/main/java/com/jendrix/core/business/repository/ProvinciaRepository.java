package com.jendrix.core.business.repository;

import org.springframework.stereotype.Repository;

import com.jendrix.common.repository.BaseCrudRepository;
import com.jendrix.core.business.entity.Provincia;

@Repository("provinciaRepository")
public interface ProvinciaRepository extends BaseCrudRepository<Provincia, Long> {
	
	public Iterable<Provincia> findByPais_Id(Long paisId);

	public Iterable<Provincia> findByPais_IdAndNombre(Long paisId, String nombre);

	public Iterable<Provincia> findByPais_IdAndNombreIgnoreCase(Long paisId, String nombre);

	public Iterable<Provincia> findByPais_IdAndNombreContaining(Long paisId, String nombre);

	public Iterable<Provincia> findByPais_IdAndNombreContainingIgnoreCase(Long paisId, String nombre);

	public Iterable<Provincia> findByPais_IdAndNombreStartingWith(Long paisId, String nombre);

	public Iterable<Provincia> findByPais_IdAndNombreStartingWithIgnoreCase(Long paisId, String nombre);
}
