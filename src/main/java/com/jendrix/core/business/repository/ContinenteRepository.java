package com.jendrix.core.business.repository;

import org.springframework.stereotype.Repository;

import com.jendrix.common.repository.BaseCrudRepository;
import com.jendrix.core.business.entity.Continente;

@Repository("continenteRepository")
public interface ContinenteRepository extends BaseCrudRepository<Continente, Long> {

	public abstract Iterable<Continente> findByNombre(String nombre);

	public abstract Iterable<Continente> findByNombreIgnoreCase(String nombre);

	public abstract Iterable<Continente> findByNombreContaining(String nombre);

	public abstract Iterable<Continente> findByNombreContainingIgnoreCase(String nombre);

	public abstract Iterable<Continente> findByNombreStartingWith(String nombre);

	public abstract Iterable<Continente> findByNombreStartingWithIgnoreCase(String nombre);

}
