package com.jendrix.core.business.repository;

import org.springframework.stereotype.Repository;

import com.jendrix.common.repository.BaseCrudRepository;
import com.jendrix.core.business.entity.TipoDocumento;

@Repository("tipoDocumentoRepository")
public interface TipoDocumentoRepository extends BaseCrudRepository<TipoDocumento, Long> {

	public Iterable<TipoDocumento> findByPais_Id(Long paisId);

	public Iterable<TipoDocumento> findByPais_IdAndNombre(Long paisId, String nombre);

	public Iterable<TipoDocumento> findByPais_IdAndNombreIgnoreCase(Long paisId, String nombre);

	public Iterable<TipoDocumento> findByPais_IdAndNombreContaining(Long paisId, String nombre);

	public Iterable<TipoDocumento> findByPais_IdAndNombreContainingIgnoreCase(Long paisId, String nombre);

	public Iterable<TipoDocumento> findByPais_IdAndNombreStartingWith(Long paisId, String nombre);

	public Iterable<TipoDocumento> findByPais_IdAndNombreStartingWithIgnoreCase(Long paisId, String nombre);
}
