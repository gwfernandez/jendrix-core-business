package com.jendrix.core.business.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.NoRepositoryBean;

import com.jendrix.common.repository.BaseCrudRepository;
import com.jendrix.core.business.entity.Persona;

@NoRepositoryBean
public interface BasePersonaRepository<T extends Persona> extends BaseCrudRepository<T, Long> {

	@Query("select t from #{#entityName} as t where t.tipoDocumento.id = ?1 and t.numeroDocumento = ?2 ")
	public T findByTipoDocumento_IdAndNumero(Long tipoDocumentoId, String numeroDocumento);
}