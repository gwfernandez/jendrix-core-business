package com.jendrix.core.business.repository;

import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import com.jendrix.core.business.entity.PersonaJuridica;

@Repository("personaJuridicaRepository")
@Transactional
public interface PersonaJuridicaRepository extends BasePersonaRepository<PersonaJuridica> {

}
