package com.jendrix.core.business.repository;

import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import com.jendrix.core.business.entity.PersonaFisica;

@Repository("personaFisicaRepository")
@Transactional
public interface PersonaFisicaRepository extends BasePersonaRepository<PersonaFisica> {

}
