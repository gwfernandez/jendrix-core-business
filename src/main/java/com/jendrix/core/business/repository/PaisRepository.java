package com.jendrix.core.business.repository;

import org.springframework.stereotype.Repository;

import com.jendrix.common.repository.BaseCrudRepository;
import com.jendrix.core.business.entity.Pais;

@Repository("paisRepository")
public interface PaisRepository extends BaseCrudRepository<Pais, Long> {

	public Iterable<Pais> findByNombre(String nombre);

	public Iterable<Pais> findByNombreIgnoreCase(String nombre);

	public Iterable<Pais> findByNombreContaining(String nombre);

	public Iterable<Pais> findByNombreContainingIgnoreCase(String nombre);

	public Iterable<Pais> findByNombreStartingWith(String nombre);

	public Iterable<Pais> findByNombreStartingWithIgnoreCase(String nombre);

	public Iterable<Pais> findByContinente_Id(Long continenteId);

	public Iterable<Pais> findByContinente_IdAndNombre(Long continenteId, String nombre);

	public Iterable<Pais> findByContinente_IdAndNombreIgnoreCase(Long continenteId, String nombre);

	public Iterable<Pais> findByContinente_IdAndNombreContaining(Long continenteId, String nombre);

	public Iterable<Pais> findByContinente_IdAndNombreContainingIgnoreCase(Long continenteId, String nombre);

	public Iterable<Pais> findByContinente_IdAndNombreStartingWith(Long continenteId, String nombre);

	public Iterable<Pais> findByContinente_IdAndNombreStartingWithIgnoreCase(Long continenteId, String nombre);

}
