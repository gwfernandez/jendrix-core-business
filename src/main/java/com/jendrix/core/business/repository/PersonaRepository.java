package com.jendrix.core.business.repository;

import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import com.jendrix.core.business.entity.Persona;

@Repository("personaRepository")
@Transactional
public interface PersonaRepository extends BasePersonaRepository<Persona> {

}
