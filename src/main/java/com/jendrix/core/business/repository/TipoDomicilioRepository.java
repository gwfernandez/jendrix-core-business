package com.jendrix.core.business.repository;

import org.springframework.stereotype.Repository;

import com.jendrix.common.repository.BaseCrudRepository;
import com.jendrix.core.business.entity.TipoDomicilio;

@Repository("tipoDomicilioRepository")
public interface TipoDomicilioRepository extends BaseCrudRepository<TipoDomicilio, Long> {

	public Iterable<TipoDomicilio> findByPais_Id(Long paisId);

	public Iterable<TipoDomicilio> findByPais_IdAndNombre(Long paisId, String nombre);

	public Iterable<TipoDomicilio> findByPais_IdAndNombreIgnoreCase(Long paisId, String nombre);

	public Iterable<TipoDomicilio> findByPais_IdAndNombreContaining(Long paisId, String nombre);

	public Iterable<TipoDomicilio> findByPais_IdAndNombreContainingIgnoreCase(Long paisId, String nombre);

	public Iterable<TipoDomicilio> findByPais_IdAndNombreStartingWith(Long paisId, String nombre);

	public Iterable<TipoDomicilio> findByPais_IdAndNombreStartingWithIgnoreCase(Long paisId, String nombre);
}
