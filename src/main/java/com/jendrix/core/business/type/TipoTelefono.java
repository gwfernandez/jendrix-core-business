package com.jendrix.core.business.type;

public enum TipoTelefono {
	// Valores.
	FIJO("Fijo"), 
	MOVIL("Movil");

	// Propiedades.
	private String label;

	private TipoTelefono(String label) {
		this.label = label;
	}

	public final String getLabel() {
		return label;
	}
}