package com.jendrix.core.business.type;

public enum TipoEstadoCivil {

	// Valores.
	SOLTERO("Soltero"), 
	CASADO("Casado"), 
	DIVORCIADO("Divorciado"), 
	VIUDO("Viudo");

	// Propiedades.
	private String label;

	private TipoEstadoCivil(String label) {
		this.label = label;
	}

	public final String getLabel() {
		return label;
	}
}