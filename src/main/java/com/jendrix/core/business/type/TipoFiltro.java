package com.jendrix.core.business.type;

public enum TipoFiltro {
	EQUALS,
	IGNORE_CASE,
	CONTAINING,
	CONTAINING_IGNORE_CASE,
	STARTING_WITH,
	STARTING_WITH_IGNORE_CASE
}
