package com.jendrix.core.business.type;

public enum TipoSexo {
	// Valores.
	MASCULINO("MASCULINO"), 
	FEMENINO("FEMENINO");

	// Propiedades.
	private String label;

	private TipoSexo(String label) {
		this.label = label;
	}

	public final String getLabel() {
		return label;
	}
}