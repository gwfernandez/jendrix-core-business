package com.jendrix.core.business.type;

public enum TipoPersona {
	PERSONA_FISICA, 
	PERSONA_JURIDICA;
}