package com.jendrix.core.business.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.jendrix.common.component.BaseConverter;
import com.jendrix.core.business.entity.Provincia;
import com.jendrix.core.business.model.ProvinciaModel;

@Component("provinciaConverter")
public class ProvinciaConverter extends BaseConverter<Provincia, ProvinciaModel> {

	@Autowired
	private PaisConverter paisConverter;

	@Override
	public Provincia toEntity(ProvinciaModel model) {
		Provincia entity = null;
		if (model != null) {
			entity = new Provincia();
			entity.setId(model.getId());
			entity.setPais(paisConverter.toEntity(model.getPais()));
			entity.setNombre(model.getNombre());
			entity.setAbreviatura(model.getAbreviatura());
		}
		return entity;
	}

	@Override
	public ProvinciaModel toModel(Provincia entity) {
		ProvinciaModel model = null;
		if (entity != null) {
			model = new ProvinciaModel();
			model.setId(entity.getId());
			model.setPais(paisConverter.toModel(entity.getPais()));
			model.setNombre(entity.getNombre());
			model.setAbreviatura(entity.getAbreviatura());
		}
		return model;
	}
}
