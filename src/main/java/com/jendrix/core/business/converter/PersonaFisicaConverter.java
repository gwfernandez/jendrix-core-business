package com.jendrix.core.business.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.jendrix.common.component.BaseConverter;
import com.jendrix.core.business.entity.PersonaFisica;
import com.jendrix.core.business.model.PersonaFisicaModel;

@Component("personaFisicaConverter")
public class PersonaFisicaConverter extends BaseConverter<PersonaFisica, PersonaFisicaModel> {

	@Autowired
	private PaisConverter paisConverter;
	@Autowired
	private TipoDocumentoConverter tipoDocumentoConverter;
	@Autowired
	private DomicilioConverter domicilioConverter;

	@Override
	public PersonaFisica toEntity(PersonaFisicaModel model) {
		PersonaFisica entity = null;
		if (model != null) {
			entity = new PersonaFisica();

			entity.setId(model.getId());
			entity.setTipoDocumento(tipoDocumentoConverter.toEntity(model.getTipoDocumento()));
			entity.setNumeroDocumento(model.getNumeroDocumento());
			entity.setDomicilios(domicilioConverter.toEntityList(model.getDomicilios()));

			entity.setApellido(model.getApellido());
			entity.setNombre(model.getNombre());
			entity.setSexo(model.getSexo());
			entity.setEstadoCivil(model.getEstadoCivil());
			entity.setNacionalidad(paisConverter.toEntity(model.getNacionalidad()));
			entity.setFechaNacimiento(model.getFechaNacimiento());
			entity.setNumeroTelefono(model.getNumeroTelefono());
			entity.setEmail(model.getEmail());
		}

		return entity;
	}

	@Override
	public PersonaFisicaModel toModel(PersonaFisica entity) {
		PersonaFisicaModel model = null;
		if (entity != null) {
			model = new PersonaFisicaModel();
			model.setId(entity.getId());
			model.setTipoDocumento(tipoDocumentoConverter.toModel(entity.getTipoDocumento()));
			model.setNumeroDocumento(entity.getNumeroDocumento());
			model.setDomicilios(domicilioConverter.toModelList(entity.getDomicilios()));

			model.setApellido(entity.getApellido());
			model.setNombre(entity.getNombre());
			model.setSexo(entity.getSexo());
			model.setEstadoCivil(entity.getEstadoCivil());
			model.setNacionalidad(paisConverter.toModel(entity.getNacionalidad()));
			model.setFechaNacimiento(entity.getFechaNacimiento());
			model.setNumeroTelefono(entity.getNumeroTelefono());
			model.setEmail(entity.getEmail());
		}
		return model;
	}
}
