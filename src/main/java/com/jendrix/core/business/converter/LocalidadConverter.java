package com.jendrix.core.business.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.jendrix.common.component.BaseConverter;
import com.jendrix.core.business.entity.Localidad;
import com.jendrix.core.business.model.LocalidadModel;

@Component("localidadConverter")
public class LocalidadConverter extends BaseConverter<Localidad, LocalidadModel> {

	@Autowired
	private PartidoConverter partidoConverter;

	@Override
	public Localidad toEntity(LocalidadModel model) {
		Localidad entity = null;
		if (model != null) {
			entity = new Localidad();
			entity.setId(model.getId());
			entity.setPartido(partidoConverter.toEntity(model.getPartido()));
			entity.setNombre(model.getNombre());
		}
		return entity;
	}

	@Override
	public LocalidadModel toModel(Localidad entity) {
		LocalidadModel model = null;
		if (entity != null) {
			model = new LocalidadModel();
			model.setId(entity.getId());
			model.setPartido(partidoConverter.toModel(entity.getPartido()));
			model.setNombre(entity.getNombre());
		}
		return model;
	}
}
