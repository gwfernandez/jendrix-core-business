package com.jendrix.core.business.converter;

import org.springframework.stereotype.Component;

import com.jendrix.common.component.BaseConverter;
import com.jendrix.core.business.entity.Continente;
import com.jendrix.core.business.model.ContinenteModel;

@Component("continenteConverter")
public class ContinenteConverter extends BaseConverter<Continente, ContinenteModel> {

	@Override
	public Continente toEntity(ContinenteModel model) {
		Continente entity = null;
		if (model != null) {
			entity = new Continente();
			entity.setId(model.getId());
			entity.setNombre(model.getNombre());
		}
		return entity;
	}

	@Override
	public ContinenteModel toModel(Continente entity) {
		ContinenteModel model = null;
		if (entity != null) {
			model = new ContinenteModel();
			model.setId(entity.getId());
			model.setNombre(entity.getNombre());
		}
		return model;
	}
}
