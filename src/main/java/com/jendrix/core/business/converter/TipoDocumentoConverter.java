package com.jendrix.core.business.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.jendrix.common.component.BaseConverter;
import com.jendrix.core.business.entity.TipoDocumento;
import com.jendrix.core.business.model.TipoDocumentoModel;

@Component("tipoDocumentoConverter")
public class TipoDocumentoConverter extends BaseConverter<TipoDocumento, TipoDocumentoModel> {

	@Autowired
	private PaisConverter paisConverter;

	@Override
	public TipoDocumento toEntity(TipoDocumentoModel model) {
		TipoDocumento entity = null;
		if (model != null) {
			entity = new TipoDocumento();
			entity.setId(model.getId());
			entity.setPais(paisConverter.toEntity(model.getPais()));
			entity.setTipoPersona(model.getTipoPersona());
			entity.setNombre(model.getNombre());
			entity.setAbreviatura(model.getAbreviatura());
		}

		return entity;
	}

	@Override
	public TipoDocumentoModel toModel(TipoDocumento entity) {
		TipoDocumentoModel model = null;
		if (entity != null) {
			model = new TipoDocumentoModel();
			model.setId(entity.getId());
			model.setPais(paisConverter.toModel(entity.getPais()));
			model.setTipoPersona(entity.getTipoPersona());
			model.setNombre(entity.getNombre());
			model.setAbreviatura(entity.getAbreviatura());
		}
		return model;
	}
}
