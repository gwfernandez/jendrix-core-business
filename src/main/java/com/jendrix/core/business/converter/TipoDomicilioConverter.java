package com.jendrix.core.business.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.jendrix.common.component.BaseConverter;
import com.jendrix.core.business.entity.TipoDomicilio;
import com.jendrix.core.business.model.TipoDomicilioModel;

@Component("tipoDomicilioConverter")
public class TipoDomicilioConverter extends BaseConverter<TipoDomicilio, TipoDomicilioModel> {

	@Autowired
	private PaisConverter paisConverter;

	@Override
	public TipoDomicilio toEntity(TipoDomicilioModel model) {
		TipoDomicilio entity = null;
		if (model != null) {
			entity = new TipoDomicilio();
			entity.setId(model.getId());
			entity.setPais(paisConverter.toEntity(model.getPais()));
			entity.setTipoPersona(model.getTipoPersona());
			entity.setNombre(model.getNombre());
		}

		return entity;
	}

	@Override
	public TipoDomicilioModel toModel(TipoDomicilio entity) {
		TipoDomicilioModel model = null;
		if (entity != null) {
			model = new TipoDomicilioModel();
			model.setId(entity.getId());
			model.setPais(paisConverter.toModel(entity.getPais()));
			model.setTipoPersona(entity.getTipoPersona());
			model.setNombre(entity.getNombre());
		}
		return model;
	}
}
