package com.jendrix.core.business.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.jendrix.common.component.BaseConverter;
import com.jendrix.core.business.entity.PersonaJuridica;
import com.jendrix.core.business.model.PersonaJuridicaModel;

@Component("personaJuridicaConverter")
public class PersonaJuridicaConverter extends BaseConverter<PersonaJuridica, PersonaJuridicaModel> {

	@Autowired
	private PaisConverter paisConverter;
	@Autowired
	private TipoDocumentoConverter tipoDocumentoConverter;
	@Autowired
	private DomicilioConverter domicilioConverter;

	@Override
	public PersonaJuridica toEntity(PersonaJuridicaModel model) {
		PersonaJuridica entity = null;
		if (model != null) {
			entity = new PersonaJuridica();

			entity.setId(model.getId());
			entity.setTipoDocumento(tipoDocumentoConverter.toEntity(model.getTipoDocumento()));
			entity.setNumeroDocumento(model.getNumeroDocumento());
			entity.setDomicilios(domicilioConverter.toEntityList(model.getDomicilios()));

			entity.setRazonSocial(model.getRazonSocial());
			entity.setNacionalidad(paisConverter.toEntity(model.getNacionalidad()));
			entity.setFechaInicioActividad(model.getFechaInicioActividad());
			entity.setNumeroTelefono(model.getNumeroTelefono());
			entity.setEmail(model.getEmail());
		}

		return entity;
	}

	@Override
	public PersonaJuridicaModel toModel(PersonaJuridica entity) {
		PersonaJuridicaModel model = null;
		if (entity != null) {
			model = new PersonaJuridicaModel();
			model.setId(entity.getId());
			model.setTipoDocumento(tipoDocumentoConverter.toModel(entity.getTipoDocumento()));
			model.setNumeroDocumento(entity.getNumeroDocumento());
			model.setDomicilios(domicilioConverter.toModelList(entity.getDomicilios()));

			model.setRazonSocial(entity.getRazonSocial());
			model.setNacionalidad(paisConverter.toModel(entity.getNacionalidad()));
			model.setFechaInicioActividad(entity.getFechaInicioActividad());
			model.setNumeroTelefono(entity.getNumeroTelefono());
			model.setEmail(entity.getEmail());
		}
		return model;
	}
}
