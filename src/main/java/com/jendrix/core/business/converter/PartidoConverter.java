package com.jendrix.core.business.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.jendrix.common.component.BaseConverter;
import com.jendrix.core.business.entity.Partido;
import com.jendrix.core.business.model.PartidoModel;

@Component("partidoConverter")
public class PartidoConverter extends BaseConverter<Partido, PartidoModel> {

	@Autowired
	private ProvinciaConverter provinciaConverter;

	@Override
	public Partido toEntity(PartidoModel model) {
		Partido entity = null;
		if (model != null) {
			entity = new Partido();
			entity.setId(model.getId());
			entity.setProvincia(provinciaConverter.toEntity(model.getProvincia()));
			entity.setNombre(model.getNombre());
		}
		return entity;
	}

	@Override
	public PartidoModel toModel(Partido entity) {
		PartidoModel model = null;
		if (entity != null) {
			model = new PartidoModel();
			model.setId(entity.getId());
			model.setProvincia(provinciaConverter.toModel(entity.getProvincia()));
			model.setNombre(entity.getNombre());
		}
		return model;
	}
}
