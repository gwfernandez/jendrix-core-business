package com.jendrix.core.business.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.jendrix.common.component.BaseConverter;
import com.jendrix.core.business.entity.Domicilio;
import com.jendrix.core.business.model.DomicilioModel;

@Component("domicilioConverter")
public class DomicilioConverter extends BaseConverter<Domicilio, DomicilioModel> {

	@Autowired
	private PaisConverter paisConverter;

	@Autowired
	private ProvinciaConverter provinciaConverter;

	@Autowired
	private PartidoConverter partidoConverter;

	@Autowired
	private LocalidadConverter localidadConverter;

	@Autowired
	private TipoDomicilioConverter tipoDomicilioConverter;

	@Override
	public Domicilio toEntity(DomicilioModel model) {
		Domicilio entity = null;
		if (model != null) {
			entity = new Domicilio();
			entity.setId(model.getId());
			entity.setTipoDomicilio(tipoDomicilioConverter.toEntity(model.getTipoDomicilio()));
			entity.setPais(paisConverter.toEntity(model.getPais()));
			entity.setProvincia(provinciaConverter.toEntity(model.getProvincia()));
			entity.setPartido(partidoConverter.toEntity(model.getPartido()));
			entity.setLocalidad(localidadConverter.toEntity(model.getLocalidad()));
			entity.setCalle(model.getCalle());
			entity.setAltura(model.getAltura());
			entity.setPiso(model.getPiso());
			entity.setDepartamento(model.getDepartamento());
			entity.setCodigoPostal(model.getCodigoPostal());
			entity.setEntreCalle1(model.getEntreCalle1());
			entity.setEntreCalle2(model.getEntreCalle2());
			entity.setObservaciones(model.getObservaciones());
		}
		return entity;
	}

	@Override
	public DomicilioModel toModel(Domicilio entity) {
		DomicilioModel model = null;
		if (entity != null) {
			model = new DomicilioModel();
			model.setId(entity.getId());
			model.setTipoDomicilio(tipoDomicilioConverter.toModel(entity.getTipoDomicilio()));
			model.setPais(paisConverter.toModel(entity.getPais()));
			model.setProvincia(provinciaConverter.toModel(entity.getProvincia()));
			model.setPartido(partidoConverter.toModel(entity.getPartido()));
			model.setLocalidad(localidadConverter.toModel(entity.getLocalidad()));
			model.setCalle(entity.getCalle());
			model.setAltura(entity.getAltura());
			model.setPiso(entity.getPiso());
			model.setDepartamento(entity.getDepartamento());
			model.setCodigoPostal(entity.getCodigoPostal());
			model.setEntreCalle1(entity.getEntreCalle1());
			model.setEntreCalle2(entity.getEntreCalle2());
			model.setObservaciones(entity.getObservaciones());
		}
		return model;
	}
}
