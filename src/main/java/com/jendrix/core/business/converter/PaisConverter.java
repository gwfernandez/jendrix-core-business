package com.jendrix.core.business.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.jendrix.common.component.BaseConverter;
import com.jendrix.core.business.entity.Pais;
import com.jendrix.core.business.model.PaisModel;

@Component("paisConverter")
public class PaisConverter extends BaseConverter<Pais, PaisModel> {

	@Autowired
	private ContinenteConverter continenteConverter;

	@Override
	public Pais toEntity(PaisModel model) {
		Pais entity = null;
		if (model != null) {
			entity = new Pais();
			entity.setId(model.getId());
			entity.setContinente(continenteConverter.toEntity(model.getContinente()));
			entity.setNombre(model.getNombre());
			entity.setNombreOficial(model.getNombreOficial());
			entity.setCodigoNumerico(model.getCodigoNumerico());
			entity.setCodigoAlfa2(model.getCodigoAlfa2());
			entity.setCodigoAlfa3(model.getCodigoAlfa3());

		}

		return entity;
	}

	@Override
	public PaisModel toModel(Pais entity) {
		PaisModel model = null;
		if (entity != null) {
			model = new PaisModel();
			model.setId(entity.getId());
			model.setContinente(continenteConverter.toModel(entity.getContinente()));
			model.setNombre(entity.getNombre());
			model.setNombreOficial(entity.getNombreOficial());
			model.setCodigoNumerico(entity.getCodigoNumerico());
			model.setCodigoAlfa2(entity.getCodigoAlfa2());
			model.setCodigoAlfa3(entity.getCodigoAlfa3());
		}
		return model;
	}
}
