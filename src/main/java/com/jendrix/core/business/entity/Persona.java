package com.jendrix.core.business.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.jendrix.core.business.type.TipoPersona;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
// @DiscriminatorColumn(name = "tipoPersonaDiscriminator", discriminatorType =
// DiscriminatorType.STRING)
@Table(name = "cb_persona", uniqueConstraints = @UniqueConstraint(columnNames = { "tipo_documento_id",
		"numero_documento" }))
@SuppressWarnings("serial")
public abstract class Persona implements Serializable {

	@Id
	@SequenceGenerator(name = "personaSEQ", sequenceName = "cb_persona_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "personaSEQ")
	@Column(name = "persona_id", unique = true)
	private Long id;

	@Enumerated(value = EnumType.STRING)
	@Column(name = "tipo_persona", nullable = false)
	private TipoPersona tipoPersona;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "tipo_documento_id", nullable = false)
	private TipoDocumento tipoDocumento;

	@Column(name = "numero_documento", length = 30, nullable = false)
	private String numeroDocumento;

	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
	@JoinColumn(name = "persona_id")
	private List<Domicilio> domicilios;

	protected Persona(TipoPersona tipoPersona) {
		this.tipoPersona = tipoPersona;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public TipoPersona getTipoPersona() {
		return tipoPersona;
	}

	@Deprecated
	// FIXME: ver de eliminar
	protected void setTipoPersona(TipoPersona tipoPersona) {
		this.tipoPersona = tipoPersona;
	}

	public TipoDocumento getTipoDocumento() {
		return tipoDocumento;
	}

	public void setTipoDocumento(TipoDocumento tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	public String getNumeroDocumento() {
		return numeroDocumento;
	}

	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}

	public List<Domicilio> getDomicilios() {
		if (domicilios == null) {
			domicilios = new ArrayList<>();
		}
		return domicilios;
	}

	public void setDomicilios(List<Domicilio> domicilios) {
		this.domicilios = domicilios;
	}

}