package com.jendrix.core.business.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.jendrix.core.business.type.TipoEstadoCivil;
import com.jendrix.core.business.type.TipoPersona;
import com.jendrix.core.business.type.TipoSexo;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
// @DiscriminatorValue(value = "PERSONA_FISICA")
@SuppressWarnings("serial")
@Table(name = "cb_persona_fisica")
public class PersonaFisica extends Persona {

	@Column(name = "apellido", length = 70)
	private String apellido;

	@Column(name = "nombre", length = 70)
	private String nombre;

	@Enumerated(EnumType.STRING)
	@Column(name = "sexo")
	private TipoSexo sexo;

	@Enumerated(EnumType.STRING)
	@Column(name = "estado_civil")
	private TipoEstadoCivil estadoCivil;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "nacionalidad_id")
	private Pais nacionalidad;

	@Column(name = "fecha_nacimiento")
	private Date fechaNacimiento;

	@Column(name = "numero_telefono", length = 20)
	private String numeroTelefono;

	@Column(name = "email", length = 100)
	private String email;

	public PersonaFisica() {
		super(TipoPersona.PERSONA_FISICA);
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public TipoSexo getSexo() {
		return sexo;
	}

	public void setSexo(TipoSexo sexo) {
		this.sexo = sexo;
	}

	public TipoEstadoCivil getEstadoCivil() {
		return estadoCivil;
	}

	public void setEstadoCivil(TipoEstadoCivil estadoCivil) {
		this.estadoCivil = estadoCivil;
	}

	public Pais getNacionalidad() {
		return nacionalidad;
	}

	public void setNacionalidad(Pais nacionalidad) {
		this.nacionalidad = nacionalidad;
	}

	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public String getNumeroTelefono() {
		return numeroTelefono;
	}

	public void setNumeroTelefono(String numeroTelefono) {
		this.numeroTelefono = numeroTelefono;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
}