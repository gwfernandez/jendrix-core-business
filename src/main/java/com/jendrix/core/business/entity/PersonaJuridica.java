package com.jendrix.core.business.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.jendrix.core.business.type.TipoPersona;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
// @DiscriminatorColumn(name = "PERSONA_JURIDICA")
@SuppressWarnings("serial")
@Table(name = "cb_persona_juridica")
public class PersonaJuridica extends Persona {

	@Column(name = "razon_social", length = 200)
	private String razonSocial;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "nacionalidad_id")
	private Pais nacionalidad;

	@Column(name = "fecha_inicio_actividad")
	private Date fechaInicioActividad;

	@Column(name = "numero_telefono", length = 20)
	private String numeroTelefono;

	@Column(name = "email", length = 100)
	private String email;

	public PersonaJuridica() {
		super(TipoPersona.PERSONA_JURIDICA);
	}

	public String getRazonSocial() {
		return razonSocial;
	}

	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}

	public Pais getNacionalidad() {
		return nacionalidad;
	}

	public void setNacionalidad(Pais nacionalidad) {
		this.nacionalidad = nacionalidad;
	}

	public Date getFechaInicioActividad() {
		return fechaInicioActividad;
	}

	public void setFechaInicioActividad(Date fechaInicioActividad) {
		this.fechaInicioActividad = fechaInicioActividad;
	}

	public String getNumeroTelefono() {
		return numeroTelefono;
	}

	public void setNumeroTelefono(String numeroTelefono) {
		this.numeroTelefono = numeroTelefono;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
}