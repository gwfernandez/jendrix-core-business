package com.jendrix.core.business.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "cb_continente")
@SuppressWarnings("serial")
public final class Continente implements Serializable {

	@Id
	@SequenceGenerator(name = "continenteSEQ", sequenceName = "cb_continente_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "continenteSEQ")
	@Column(name = "continente_id", unique = true)
	private Long id;

	@Column(name = "nombre", length = 100, unique = true)
	private String nombre;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
}