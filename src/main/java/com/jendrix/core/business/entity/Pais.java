package com.jendrix.core.business.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "cb_pais")
@SuppressWarnings("serial")
public final class Pais implements Serializable {

	@Id
	@SequenceGenerator(name = "paisSEQ", sequenceName = "cb_pais_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "paisSEQ")
	@Column(name = "pais_id", unique = true)
	private Long id;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "continente_id", nullable = false)
	private Continente continente;

	@Column(name = "nombre", length = 50, unique = true, nullable = false)
	private String nombre;

	@Column(name = "nombre_oficial", length = 100)
	private String nombreOficial;

	@Column(name = "codigo_numerico", length = 3, unique = true, nullable = false)
	private String codigoNumerico;

	@Column(name = "codigo_alfa2", length = 2, unique = true, nullable = false)
	private String codigoAlfa2;

	@Column(name = "codigo_alfa3", length = 3, unique = true, nullable = false)
	private String codigoAlfa3;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Continente getContinente() {
		return continente;
	}

	public void setContinente(Continente continente) {
		this.continente = continente;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getNombreOficial() {
		return nombreOficial;
	}

	public void setNombreOficial(String nombreOficial) {
		this.nombreOficial = nombreOficial;
	}

	public String getCodigoNumerico() {
		return codigoNumerico;
	}

	public void setCodigoNumerico(String codigoNumerico) {
		this.codigoNumerico = codigoNumerico;
	}

	public String getCodigoAlfa2() {
		return codigoAlfa2;
	}

	public void setCodigoAlfa2(String codigoAlfa2) {
		this.codigoAlfa2 = codigoAlfa2;
	}

	public String getCodigoAlfa3() {
		return codigoAlfa3;
	}

	public void setCodigoAlfa3(String codigoAlfa3) {
		this.codigoAlfa3 = codigoAlfa3;
	}
}